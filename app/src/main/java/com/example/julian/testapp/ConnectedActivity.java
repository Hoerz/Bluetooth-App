package com.example.julian.testapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ConnectedActivity extends Activity {

    private BleHandle mBleHandle;
    private int fanSpeed = 0;
    private int mFanStepsize;
    private static ConnectedActivity ourInstance = null;
    private TextView mTemperatureValue;
    private TextView mHumidityValue;
    private Context mContext;
    private float mTemp = 0;
    private float mHumidity = 0;

    static ConnectedActivity getInstance() {
        return ourInstance;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connected_main);
        Log.d("LOG", "Connected Activity");

        mBleHandle = BleHandle.getInstance();

        mBleHandle.discoverServices();

        mFanStepsize = 65535/100;

        this.ourInstance = this;

        mContext = this;

        mTemperatureValue = (TextView) findViewById(R.id.temperatureValue);
        mHumidityValue = (TextView) findViewById(R.id.humidityValue);

        SeekBar sk=(SeekBar) findViewById(R.id.seekBar);
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                if(progress != fanSpeed){
                    fanSpeed = progress;
                    //Log.d("LOG","New Fanspeed: " + fanSpeed);
                    short temp = (short) (progress * mFanStepsize);
                    mBleHandle.setFanSpeed(temp);
                }

            }


        });



        }


    public void updateHumidity(float humidity){
        mHumidity = humidity;
        Log.d("LOG", "Humidity: ");


        runOnUiThread(new Runnable() {
            public void run(){
                mHumidityValue.setText(mHumidity+" %");
            }
        });
    }


    public void updateTemp(float temp){
        mTemp = temp;
        Log.d("LOG","Temperatur: " + temp + " °C");

        runOnUiThread(new Runnable() {
            public void run(){
                mTemperatureValue.setText(mTemp+" °C");
            }
        });

    }

}
