package com.example.julian.testapp;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

class DeviceList {

    private List<BluetoothDevice> devices = new ArrayList<>();

    private static final DeviceList ourInstance = new DeviceList();

    static DeviceList getInstance() {
        return ourInstance;
    }

    private DeviceList() {
    }

    public List<BluetoothDevice> getDevices() {
        return devices;
    }

    public void add(BluetoothDevice item) {
        int deviceInTheList = 0;
        //Log.d("LOG","Adding....");
        for(int i = 0; i < this.devices.size(); i++){
            if(item.getAddress().equals(devices.get(i).getAddress())){
                deviceInTheList = 1;
            }
        }

        if(deviceInTheList == 0) {
            this.devices.add(item);
            BluetoothFragment.getInstance().updateView();
        }
    }

    public void remove(BluetoothDevice item) {
        this.devices.remove(item);
    }
}
