package com.example.julian.testapp;



import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BluetoothFragment.OnListFragmentInteractionListener {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private BleHandle mBleHandle;
    private DeviceList mDeviceList = DeviceList.getInstance();
    private boolean scanning = false;
    private static MainActivity ourInstance = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mBleHandle = new BleHandle(getApplicationContext());
        mBleHandle.initialize();


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        BluetoothFragment bluetoothFragment = BluetoothFragment.getInstance();
        mFragmentTransaction.replace(R.id.content_frame, bluetoothFragment);
        mFragmentTransaction.commit();

        MainActivity.ourInstance = this;

    }

    public static MainActivity getInstance(){
        return ourInstance;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.toggle_scan) {
            if(scanning){
                scanning = false;
                mBleHandle.triggerScan(scanning);
                item.setTitle("Start Scan");
            }
            else{
                scanning = true;
                mBleHandle.triggerScan(scanning);
                item.setTitle("Stop Scan");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onListFragmentInteraction(BluetoothDevice item) {
        Log.d("LOG","Ich will connecten....");
        mBleHandle.connect(item);
    }


    public void onBluetoothConnected(){
        Intent i = new Intent(MainActivity.this,ConnectedActivity.class);
        startActivity(i);
    }
}
